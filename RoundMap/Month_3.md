Third month (Permissions)
------

Our application will now been usable with a minimum of features.    
We can now focus on the admin account.    

Admin will be a particular account type (basic and childs are    
the other versions) and will be able to manages multiples childs    
accounts.        

New features that come with admin are:
- Create child account associated with current
- Create Safe and archive associated with a specific child account
- Limit child account safes creation (quota)
- Edit, delete, block child account.
- Create all this features on Mobile APP