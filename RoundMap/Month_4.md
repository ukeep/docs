Fourth month (Providers)
----

After stabilize all the permissions features we need to ensure that    
mobile application is well design and responsive. Multiples tests    
for different devices size will be done and fix if needed.    

We will provide more providers for backuping data like Amazon or    
OVH who offer same type of backup that Online.

For the moment only "cold" storage is supported, so we    
need to support other backup methods like S3.    
A new option for backup creation will be available:    
`Backup type`.   
It will be possible to choose between:
- Cold storage (high security and long term backup)
- Hot storage (high availability and speed)
- Create all this features on Mobile APP too
