Second month (Integration)
-----

After writing the core of our service, we need to ensure that     
everythings will be still working with our new features.     
This is why integrations tests will guarante us a minimum of bugs.    

Gitlabs tools will be uses to automate our tests, syntax checks ...   
and merge branchs without surprise.    
Our infrastructure will be migrate to docker when developpement    
workflow will be stable (small amount of bug in dev / preprod).    
2 version will be live, one at `dev.api.ukeep.fr` and other at    
`api.ukeep.fr`.

The TODO list will be the following:    
- Pinned safes and archive on main dashboard
- Interractive archive list with custom and powerfull filters
- Archive and safes info details
- Server list status page (only 'Online' for the moment)
- Start App mobile developement from the POC with the same    
  features as Web APP