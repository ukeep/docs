First month (Core)
------

Before starting serious stuff, we need to establish how we    
will working as a group of 4 developpers.
Multiples things must be clarified:
- Our workflow (git, tests suite, dailty todo, platform used    
  for communicate, ...)
- Technologies that will be used
- How features will be allocated
- List of fonctionnal documentation files to write
- List of features of each components (API, Front, mobile App)

So the major things to do on the first month will be all the 4    
steps listed before.    
And in second time, we will start to write 3 proof of concept    
components, one for the front-end, one for the API and one last    
for the mobile app.    

Continious integration and a flexible infrastructure will be    
the earth of our solution, this is why we will need to start    
the structure of our service.    

After writing documentations files and dividied tasks, we    
can start developping the core of front and backend    
application.    
The frontend application will contain pages for:    
- Login
- Signup
- Main dashboard (with nothing for the moment)
- Safe list
- Archives list 

Routes corresponding to thoses pages will be done on the API.    
Mobile app will be develop too with all thoses basics features.