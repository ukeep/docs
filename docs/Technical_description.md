Technical description
------

Ukeep project will be compose 4 open sources projects:
- [API](https://gitlab.com/ukeep/api)
- [Web App](https://gitlab.com/ukeep/desktop-app)
- [Mobile App](https://gitlab.com/ukeep/mobile-app)
- [Website](https://gitlab.com/ukeep/website)
- [Documentation](https://gitlab.com/ukeep/docs)

The API will use the following technologies:
- NodeJS (>= 10.3)
- Javascript ES2018
- NPM (>= 6.2)
- PostgreSQL (>= 10)
- Express (>= 4.16)

The web App will use:
- Angular (>= 7)
- Javascript ES2018
- Electron (>= 4.1)
- NodeJS (>= 10.3)
- NPM (>= 6.2)

Mobile App will use:
- React native (>= 0.59)
- NodeJS (>= 10.3)
- NPM (>= 6.2)

And Website:
- Angular (>= 7)
- Javascript ES2018
- NodeJS (>= 10.3)
- NPM (>= 6.2)
- Nginx (>= 2)

Sentry will be used for log all errors on our services, it's     
a powerfull tools to maintain and proceed errors.    
Eslint will also be used with differents configuration for all    
the projects syntax check.

Gitlab is our official platform release, a group with all    
projects has been created [here](https://gitlab.com/ukeep).    
A `.gitlab-ci.yml` file will be created for every projects    
with pipeline and runners.    
The goal is to provide an automatic and fast way to test    
and correct our projects.
