Features
------

### Basic
- Connect to multiple storages providers account and get all informations    
  (user personals informations, Stockage, S3 archives)
- Main dashboard with pinned links (favourite safe, ...), and data charset summary
- List of remote servers status

- Cold storage:
  * List safes and archives
  * Create, edit, delete safe
  * Create, edit, delete archive

- Hot storage:
  * List buckets
  * Create edit and detele bucket

- Choose a favourite backup endpoint
- Create schedule backup (daily, monthly, ...)
- Configure and personalize the interface (with default setup, basic, avanced expert)

### Admin
- admin account:
  * List, add, edit "children" account
  * Create mandatory safes and archive
  * Manage safes / archives childrens quotas

### Self hosting
- Self host a S3 and cold storage server
- Manage clients:
  * List, add, edit and delete clients
  * Add quotas
