Validation process
------

Each ukeep projects will have 1 maintainer.   
Validation will be done by one member of the service. This will be the one allowed    
to merge requests to master branch.    

The maintainer must define severals rules before for starting the project:    
- General documentations describing project (workflow, environment, goals, ..)
- Documentation for the languages used (with version)
- esling configuration file
- Roundmap for 6 months
- CONTRIBUTE.md, README.md and LISENCE.md files

After that the owner must create template architecture of the project before    
any developper come help him.    