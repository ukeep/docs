Constraints
------

The project team will must have perfect control of used technologies, it must be    
strong and error free.    

Ukeep will use the minimum dependencies as possible to provide secure and easily    
maintainable applications.    

The core application should remain simple. NodeJS dependencies are a pain, we    
must choose each package wisely.    

The main constraint will be the backup integrity. Users must choose us for the    
reliability of keeping their datas. This is the main purpose of our service.    

To prevent some troubles with user's datas, a major monitoring system need to be    
created. All provider's API will be controlled every day to preserve the    
availabilty and the integrity of user's datas.

Users comments and wishes will be also very relevant, and we should care about    
every complaints about their personal experiences and doubts.    
