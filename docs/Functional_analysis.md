Functional analysis
------

Basic account:
```
					         	         --> Add safe
							      /   --> Delete safe
					         	    /   / 
			               ---> List safes ----				  --> Add archive
				     /		            \   \ 			/
				   /			      \	  --> Choose safe -----  --> Delete archive
				 /		                --> Edit safe --> Edit name
			       /
Login page --> Main dashboard ------> User settings --> Edit profile
			       \
				 \ 
				   --> Servers lists --> Choose server --> Information servers (status, latency... )
```

---

Admin account:
```
			  	                   	                        --> Add safe
			  	             		                     /   --> Delete safe
			                           	                   /   / 
			                      ---> List personal safes ---  --				  --> Add archive
			  	           /		                   \   \ 			/
			  	         /			             \	  --> Choose safe -----  --> Delete archive
			  	       /		                        --> Edit safe --> Edit name
				     /                                                                    
			           /                                                                    --> Add safe
			         /			   --> Add child      --> Add plan backup    /   --> Delete safe
			       /		 	 /		    /                      /   / 
Login page --> Main dashboard  --> Child account list -- --> Edit child  -- --> list safes  ------  --      		          --> Add archive
			       \			 \		                           \   \ 			/
				 \ 			   --> Delete child                          \	  --> Choose safe -----  --> Delete archive
				   \                                                                   --> Edit safe --> Edit name
				     \                                                                
				       --> Servers lists --> Choose server --> Information servers (status, latency... )
```

"Child" account:
```
					         	         --> Add safe
							      /   --> Delete safe
					         	    /   / 
			               ---> List safes ----				  --> Add archive
				     /		            \   \ 			/
				   /			      \	  --> Choose safe -----  --> Delete archive
				 /		                --> Edit safe --> Edit name
			       /
Login page --> Main dashboard ------> User settings --> Edit profile
```
