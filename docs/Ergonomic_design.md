Ergonomic_design
---

## Start App

When an user start the application, if he is not connected, Ukeep shows up a    
login page. The user has the choice to create an account if he does not already    
have one


### Sign in and Sign up
<div align="center">
    <img src="/.resources/pics/templates_pics/1_SI.png" alt="SignIn" />
    <img src="/.resources/pics/templates_pics/2_SU.png" alt="SignUp" />
</div>

<div align="center">
    The sign in an sign up page are pretty basic, the user can find multiples inputs    
    for his informations : login / password / email / etc...
</div>

Dashboard
-----
<div align="center">
    <img src="/.resources/pics/templates_pics/3_dashboard.png" alt="Dashboard" />
</div>

<div align="center">
    The dashboard is the showroom feature of Ukeep application, the user can
    find here an overview of all Ukeep uses, statistics, backup summaries, etc..
</div>

Vaults module
-----
The vault module is made of two parts : a list page of different vaults, and a    
detail page that gives more information about a selected vault and allows you    
to edit it.

### List and details
<div align="center">
    <img src="/.resources/pics/templates_pics/4_vault_list.png" alt="VaultList" />
    <img src="/.resources/pics/templates_pics/5_vault_details.png" alt="VaultDetails" />
</div>

<div align="center">
    The list page displays basic informations about a vault and allows the user    
    to remove the selected vault or edit it.
</div>

<div align="center">
    The detail page allows the user to modify the vault name and description.
</div>

Backups module
-----
The backup module is made of two parts : a list page of different backups, and a    
detail page that gives more informations about a selected backup and allows you    
to edit it.

### List and details
<div align="center">
    <img src="/.resources/pics/templates_pics/6_backup_list.png" alt="BackupList" />
    <img src="/.resources/pics/templates_pics/7_backup_details.png" alt="BackupDetails" />
</div>

<div align="center">
    The list page displays basic informations about a backup and allows the user    
    to remove the selected backup or edit it.
</div>

<div align="center">
    The detail page allows the user to modify the backup name, description, the    
    folder to backup and the optional backup frenquency.
</div>

Settings
-----
<div align="center">
    <img src="/.resources/pics/templates_pics/8_settings.png" alt="Dashboard" />
</div>

<div align="center">
    The settings page is where the user can find his preferences, set and change    
    information or settings, upload a profile picture and of course the ability    
    to delete his account.
</div>