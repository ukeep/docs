Authentication
------

Ukeep will not provide a centralized authentication system, we don't want    
to clutter user with another couple of credentials (and an external database / api).    

Backup providers dispose of their own authentication system and we want to    
offer all of them for account connection.    
On the main page you will have a select list with all of the providers we can    
handle, after chosing one, every authentication type the providers offer will    
be display (Oauth, Bearer, custom ...)