Project need and objectifs
-----

Ukeep need to provide a trustable, and open source backup service.    
It will rely on a flexible and performant team for developping a simple and      
robust platform.    
The main strenght to provide a open source service, is the full disclosure to    
its users and the backing or a community.

The project will need an open source platform and a launch page site to describe     
itself and be available by anyone.    
Server side application will need a proper infrastructure with performance to    
handle user client request and provider API call.    
The server will be launchable from any device who suite the recommended    
environment and performances characteristics.
