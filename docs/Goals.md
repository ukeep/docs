Goals
-------

The main objectif is to have a working and robust system.    
We don't want to replicate ten times the data.    
Instead we will concentrate on 3 data store points with the most reliability.

The priority will be the monitoring of data transit and consistent.    
Our providers availabilty will be our daily preocupation.    

Ukeep KPI:
- Providers servers status
- Users traffic
- Providers API performance (response time)
- Total ammount of data in To by month

The main idea is to provide a simple way for backuping.    
But many ways exist for backuping: Cold, Fast, Persistent, Decentralized, ...    
The goals is to provide all of thoses in one interface.   

API must be hostable easily on multiples devices.    
From the computer who run the front to a remote baremetal server.   
API will provides routes to communicate directly with providers.    
Following your backup setting, the API will choose how and where    
your data will be backup.

The application is light, a minimum of dependencies have been     
installed to consume a minimum of RAM.    
Front application can adapt the display following your level    
of settings you want.    
Features will be plugable easly to suite your needs.
