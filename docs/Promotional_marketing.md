# Website
Ukeep has its own website that resume the application goals, provide the app    
for download, and let users to communicate with the Ukeep team.

[Go to Ukeep website](https://ukeep.fr)

# Gitlab community
The whole project is open source, hosted on our own Gitlab server.

[Ukeep Gitlab group](https://gitlab.com/ukeep)

# Twitter
We have a Twitter account to increase the audience reached.