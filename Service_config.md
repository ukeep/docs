Service configuration
------

In gitlab
---

`Settings` => `Repository`:
- Do not allow users to remove git tags with git push
- Prevent commiting secrets to Git
- Create `.gitlab-ci.yml` with:
  * npm initialization
  * Test run (lint and functionnal tests)
  * Project start

`Settings` => `Operations`:
- Active error tracking
- Fill API url with : `https://sentry.io`
- In sentry go [here](https://sentry.io/settings/account/api/auth-tokens/)
- Create token and copy it in gitlab auth token field
- Click on connect then choose destination project for loggin

`Settings` => `CI / CD`:
- Uncheck => `General pipelines` => `Public pipelines`
- Click on `Runners` => `Disable shared Runners`
- Set up personnal runner
- On a linux machine, install docker, instrction [here](https://docs.gitlab.com/runner/install/)
- Edit runner and check `Run untagged jobs` then save changes
- Follow pipelines / jobs [here](https://gitlab.com/ukeep/api/pipelines)

In Sentry
---
Create organisation, then projects in it.
For each project:

`Settings` => `Rules` => `New rule`:
Create new rule for sending notifications when conditions match    
Choose a specific environment for the default production notification.

`Settings` => `Environments`:
Set `dev` for default env