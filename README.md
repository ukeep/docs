# Ukeep documentations</center>

## [UPDATE] 9 may 2019: Ukeep API code will be move directly in front APP description [here](Update_09_05_2019.md)

### What ?
Ukeep is a multiplatform application that makes backups easier.

### Why ?
It figures out that today, backup solutions are not so easy and fast to use or host as we would like.    
A desktop application connected to any sort of backup backend will save you some time.

### Where ?
Everywhere, you can find the application on each systems, computer and mobile.    
The Ukeep API can be hosted at home or on a remote server.

### For who ?
Everyone: startup, big business, family, anyone who needs persistent backups.

Summary
------
- [Validation process](docs/Validation_process.md)
- [Project needs and objectifs](docs/Project_needs_objectifs.md)
- [Users needs](docs/Users_needs.md)
- [Target](docs/Target.md)
- [Feature](docs/Feature.md)
- [Goals](docs/Goals.md)
- [Functional analysis](docs/Functional_analysis.md)
- [Constraints](docs/Constraints.md)
- [Grahic design](docs/Grahic_design.md)
- [Ergonomic design](docs/Ergonomic_design.md)
- [Technical description](docs/Technical_description.md)
- [Promotional marketing](docs/Promotional_marketing.md)

Roundmap:
------

- [First month](RoundMap/Month_1.md)
- [Second month](RoundMap/Month_2.md)
- [Third month](RoundMap/Month_3.md)
- [Fourth month](RoundMap/Month_4.md)
- [Fifth month](RoundMap/Month_5.md)

Team:
------

| Pictures                                                                                | Name     | Role     | Git[hub/lab]                                 |
|-----------------------------------------------------------------------------------------|----------|----------|-------------------------------------|
|<img src="/.resources/pics/profile_pictures/bbichero.jpg" alt="Kitten" height="150px" /> | Baptiste | Backend  |[Go to](https://gitlab.com/bbichero) |
|<img src="/.resources/pics/profile_pictures/romontei.jpg" alt="Kitten" height="150px" /> | Robin    | Frontend |[Go to](https://github.com/romontei) |
|<img src="/.resources/pics/profile_pictures/nbeny.jpg"    alt="Kitten" height="150px" /> | Nicolas  | Frontend |[Go to](https://github.com/nbeny)    |
|<img src="/.resources/pics/profile_pictures/aroger.jpg"   alt="Kitten" height="150px" /> | Antoine  | Backend  |[Go to](https://gitlab.com/aroger)   |
